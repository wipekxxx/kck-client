export const environment = {
  production: true,
  apiUrl: 'https://kck-server.herokuapp.com/api/',
  authApiUrl: 'https://kck-server.herokuapp.com/oauth/token'
};
