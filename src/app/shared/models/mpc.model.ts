import {Pad} from './pad.model';
import {SocialUser} from 'angular5-social-login';

export class Mpc {
  id: string;
  pads?: Pad[];
  // padsChars?: string[];
  name?: string;
  describe?: string;
  author?: string;
  tags?: string[];
  user?: SocialUser;
}
