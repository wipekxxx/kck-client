import {Account} from './account.model';
import {TransferType} from './transfer-type.model';
import {Beneficiary} from './beneficiary.model';

export class Transfer {
  id?: string;
  transactionDate?: Date;
  title?: string;
  amount?: number;
  type?: TransferType;
  beneficiary?: Beneficiary;
  account?: Account;
}
