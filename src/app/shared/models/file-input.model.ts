export class FileInput {
  private _fileNames;

  constructor(private _files: File[], private _base64encodedFile, private delimiter: string = ', ') {
    this._base64encodedFile = _base64encodedFile;
    if (_files.length === 1) {
      this._fileNames = _files[0].name;
    } else if (_files.length > 1) {
      this._fileNames = this._files.map((f: File) => f.name).join(delimiter);
    } else {
      this._fileNames = _base64encodedFile != null ? 'Text' : 'aaa';
    }
  }

  get files() {
    return this._files || [];
  }

  get fileNames(): string {
    return this._fileNames;
  }

  get base64encodedFile(): string {
    return this._base64encodedFile;
  }

  set base64encodedFile(value: string) {
    this._base64encodedFile = value;
  }
}
