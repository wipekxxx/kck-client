import {FileInput} from './file-input.model';

export class Pad {
  id?: string;
  padChar?: string;
  padText?: string;
  file?: FileInput;
  base64encodedFile?: string;
}
