export class Base64Encoder {
  // context = new AudioContext();
  // source = null;
  // audioBuffer = null;

  // Converts an ArrayBuffer to base64, by converting to string
  // and then using window.btoa' to base64.
  bufferToBase64(buffer) {
    const bytes = new Uint8Array(buffer);
    const len = buffer.byteLength;
    let binary = '';
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
  };

  // base64ToBuffer(buffer) {
  //   const binary = window.atob(buffer);
  //   const aBuffer = new ArrayBuffer(binary.length);
  //   const bytes = new Uint8Array(aBuffer);
  //   for (let i = 0; i < buffer.byteLength; i++) {
  //     bytes[i] = binary.charCodeAt(i) & 0xFF;
  //   }
  //   console.log(buffer);
  //   return buffer;
  // };
  // stopSound() {
  //   if (this.source) {
  //     this.source.stop(0);
  //   }
  // }

  // getBase64EncodedString(file) {
  //   const reader = new FileReader();
  //   console.log(file);
  //   console.log(this.bufferToBase64(reader.readAsArrayBuffer(file)));
  //   return this.bufferToBase64(reader.readAsArrayBuffer(file));
  // }

//   playSound() {
//     // source is global so we can call .stop() later.
//     source = context.createBufferSource();
//     source.buffer = audioBuffer;
//     source.loop = false;
//     source.connect(context.destination);
//     source.start(0); // Play immediately.
//   }
//   function initSound(arrayBuffer) {
//     var base64String = bufferToBase64(arrayBuffer);
//     var audioFromString = base64ToBuffer(base64String);
//     document.getElementById("encodedResult").value=base64String;
//     context.decodeAudioData(audioFromString, function (buffer) {
//       // audioBuffer is global to reuse the decoded audio later.
//       audioBuffer = buffer;
//       var buttons = document.querySelectorAll('button');
//       buttons[0].disabled = false;
//       buttons[1].disabled = false;
//     }, function (e) {
//       console.log('Error decoding file', e);
//     });
//   }
// // User selects file, read it as an ArrayBuffer and pass to the API.
//   var fileInput = document.querySelector('input[type="file"]');
//   fileInput.addEventListener('change', function (e) {
//     var reader = new FileReader();
//     reader.onload = function (e) {
//       initSound(this.result);
//     };
//     reader.readAsArrayBuffer(this.files[0]);
//   }, false);
// // Load file from a URL as an ArrayBuffer.
// // Example: loading via xhr2: loadSoundFile('sounds/test.mp3');
//   function loadSoundFile(url) {
//     var xhr = new XMLHttpRequest();
//     xhr.open('GET', url, true);
//     xhr.responseType = 'arraybuffer';
//     xhr.onload = function (e) {
//       initSound(this.response); // this.response is an ArrayBuffer.
//     };
//     xhr.send();
//   }
}

