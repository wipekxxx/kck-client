import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from './material-module/material.module';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import { InputFileComponent } from './components/input-file/input-file.component';
import {ByteFormatPipe} from './pipes/byte-format.pipe';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    MaterialModule,
    InputFileComponent,
    ByteFormatPipe
  ],
  declarations: [InputFileComponent, ByteFormatPipe],
  providers: [ByteFormatPipe]
})
export class SharedModule {
}
