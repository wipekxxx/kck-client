import {AbstractControl, FormControl, ValidatorFn} from '@angular/forms';
import {MPC_FILES_EXT} from '../../app.consts';

export function fileTypeValidator(control: FormControl) {
  if (control.value != null && control.value.files != null) {
    const fileExt = control.value.files[0].type;
    const condition = MPC_FILES_EXT.filter(i => i === fileExt).length === 0;
    if (condition) {
      return {
        fileFormat: {valid: false}
      };
    }
    return null;
  }
}

export function editFileTypeValidator(control: FormControl) {
  if (control.value != null && control.value.files != null) {
    if (control.value && control.value.base64encodedFile) {
      return null;
    }

    const fileExt = control.value.files[0].type;
    const condition = MPC_FILES_EXT.filter(i => i === fileExt).length === 0;
    if (condition) {
      return {
        fileFormat: {valid: false}
      };
    }
    return null;
  }
}
