import {AbstractControl, ValidatorFn} from '@angular/forms';
import {FileInput} from '../models/file-input.model';

export class FileValidators {

  /**
   * Function to control content of files
   *
   * @param control
   *
   * @returns
   */
  static maxContentSize(bytes: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {

      // const size = control && control.value ? (control.value as FileInput).files.map(f => f.size).reduce((acc, i) => acc + i, 0) : 0;
      let fileSize = 0;
      if (control.value && control.value.files) {
        fileSize = control.value.files[0].size;
      }

      const size = control && control.value ? fileSize : 0;
      const condition = bytes > size;
      return condition ? null : {
        maxContentSize: {
          actualSize: size,
          maxSize: bytes
        }
      };
    }
  }

  static maxContentEditSize(bytes: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      if (control.value && control.value.base64encodedFile) {
        return null;
      }
      // const size = control && control.value ? (control.value as FileInput).files.map(f => f.size).reduce((acc, i) => acc + i, 0) : 0;
      let fileSize = 0;
      if (control.value && control.value.files) {
        fileSize = control.value.files[0].size;
      }

      const size = control && control.value ? fileSize : 0;
      const condition = bytes > size;
      return condition ? null : {
        maxContentSize: {
          actualSize: size,
          maxSize: bytes
        }
      };
    }
  }
}
