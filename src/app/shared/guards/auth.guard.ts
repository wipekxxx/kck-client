import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {tokenNotExpired} from 'angular2-jwt';
import {UsersService} from '../../services/users.service';
import {LOCAL_STORAGE_GMAIL_USER_FIELD_NAME, TOKEN_NAME} from '../../app.consts';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private usersService: UsersService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (localStorage.getItem(LOCAL_STORAGE_GMAIL_USER_FIELD_NAME) != null) {
      return true;
    } else {
      this.router.navigate(['home/welcome'], {queryParams: {redirectTo: state.url}});
      return false;
    }
    // if (tokenNotExpired(TOKEN_NAME, this.usersService.accessToken)) {
    //   return true;
    // } else {
    //   this.router.navigate(['login'], {queryParams: {redirectTo: state.url}});
    //   return false;
    // }
  }
}
