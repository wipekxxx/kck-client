import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';
import {TOKEN_NAME} from '../../app.consts';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  excludedPaths = [environment.apiUrl, environment.apiUrl + 'users/register'];

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.excludedPaths.includes(request.url)) {
      const token = localStorage.getItem(TOKEN_NAME);
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + token
        }
      });
    }
    return next.handle(request);
  }


}
