import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NewMpcComponent} from './new-mpc/new-mpc.component';
import {MpcRoutingModule} from './mpc.routing';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {MpcPadComponent} from './new-mpc/mpc-pad/mpc-pad.component';
import {PadModalComponent} from './new-mpc/pad-modal/pad-modal.component';
import {NewMpcService} from '../services/new-mpc.service';
import {MpcPropertiesModalComponent} from './new-mpc/mpc-properties-modal/mpc-properties-modal.component';
import {SaveMpcService} from '../services/save-mpc.service';
import {MpcConsoleComponent} from './mpc-console/mpc-console.component';
import {MpcConsoleService} from '../services/mpc-console.service';
import {MpcConsolePadComponent} from './mpc-console/mpc-console-pad/mpc-console-pad.component';
import {MpcListComponent} from './mpc-list/mpc-list.component';
import {MpcCardComponent} from './mpc-list/mpc-card/mpc-card.component';
import {MpcListFilterPipe} from './mpc-list/mpc-list-filter.pipe';
import {MpcListTagsPipe} from './mpc-list/mpc-list-tags.pipe';
import {UserMpcListPipe} from './mpc-list/user-mpc-list.pipe';
import {MpcEditComponent} from './mpc-edit/mpc-edit.component';
import {MpcEditPadComponent} from './mpc-edit/mpc-edit-pad/mpc-edit-pad.component';
import {MpcEditPropertiesModalComponent} from './mpc-edit/mpc-edit-properties-modal/mpc-edit-properties-modal.component';
import {PadEditModalComponent} from './mpc-edit/pad-edit-modal/pad-edit-modal.component';
import {EditMpcService} from '../services/edit-mpc.service';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MpcRoutingModule
  ],
  exports: [MpcListComponent],
  declarations: [
    NewMpcComponent,
    MpcPadComponent,
    PadModalComponent,
    MpcPropertiesModalComponent,
    MpcConsoleComponent,
    MpcConsolePadComponent,
    MpcListComponent,
    MpcCardComponent,
    MpcListFilterPipe,
    MpcListTagsPipe,
    UserMpcListPipe,
    MpcEditComponent,
    MpcEditPadComponent,
    MpcEditPropertiesModalComponent,
    PadEditModalComponent],
  entryComponents: [PadModalComponent, MpcPropertiesModalComponent, MpcEditPropertiesModalComponent, PadEditModalComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    NewMpcService,
    SaveMpcService,
    MpcConsoleService,
    MpcListFilterPipe,
    MpcListTagsPipe,
    UserMpcListPipe,
    EditMpcService]
})
export class MpcModule {
}
