import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpcListComponent } from './mpc-list.component';

describe('MpcListComponent', () => {
  let component: MpcListComponent;
  let fixture: ComponentFixture<MpcListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpcListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpcListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
