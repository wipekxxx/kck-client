import {Pipe, PipeTransform} from '@angular/core';
import {Mpc} from '../../shared/models/mpc.model';

@Pipe({
  name: 'mpcListTags'
})
export class MpcListTagsPipe implements PipeTransform {

  transform(mpcs: Mpc[], tag: string): Mpc[] {
    if (!mpcs) {
      return [];
    }
    if (!tag) {
      return mpcs;
    }

    tag = tag.toLowerCase();

    return mpcs.filter(mpc => {
      return mpc.tags.map(t => t['name']).includes(tag);
    });
  }
}
