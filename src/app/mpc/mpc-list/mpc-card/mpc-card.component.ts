import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Mpc} from '../../../shared/models/mpc.model';
import {UsersService} from '../../../services/users.service';
import {LOCAL_STORAGE_GMAIL_USER_FIELD_NAME} from '../../../app.consts';
import {MpcConsoleService} from '../../../services/mpc-console.service';

@Component({
  selector: 'app-mpc-card',
  templateUrl: './mpc-card.component.html',
  styleUrls: ['./mpc-card.component.scss']
})
export class MpcCardComponent implements OnInit, AfterViewInit {

  animations = [
    'bounce',
    'flash',
    'pulse',
    'rubberBand',
    'shake',
    'swing',
    'tada',
    'wooble',
    'jello'
  ];

  colors = [
    'default-color',
    'primary-color',
    'secondary-color',
    'default-color-dark',
    'primary-color-dark',
    'secondary-color-dark',
    'elegant-color',
    'elegant-color-dark',
    'stylish-color',
    'stylish-color-dark',
    'unique-color',
    'unique-color-dark',
    'special-color-dark'
  ];

  @Input('mpc')
  mpc: Mpc;

  @Output()
  mpcRemoved = new EventEmitter<null>();

  headerColor: string;
  footerColor: string;
  playButtonColor: string;
  playButtonAnimation: string;

  canBeEditable = false;

  constructor(private mpcConsoleService: MpcConsoleService) {
    this.headerColor = this.getRandomColor();
    this.footerColor = this.getRandomColor();
    this.playButtonColor = this.getRandomColor();
    this.playButtonAnimation = this.getRandomAnimation();
  }

  ngOnInit() {
    setTimeout(() => {
      const user = JSON.parse(localStorage.getItem(LOCAL_STORAGE_GMAIL_USER_FIELD_NAME));
      this.canBeEditable = this.mpc.user != null && user != null && this.mpc.user.id === user.id;
    }, 250);
  }

  getRandomAnimation() {
    return this.animations[Math.floor(Math.random() * this.animations.length)];
  }

  getRandomColor() {
    return this.colors[Math.floor(Math.random() * this.colors.length)];
  }

  ngAfterViewInit(): void {

  }

  remove() {
    this.mpcConsoleService.removeMpcById(this.mpc.id);
    this.mpcRemoved.emit();
  }
}
