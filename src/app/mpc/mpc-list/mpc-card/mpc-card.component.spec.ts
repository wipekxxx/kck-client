import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpcCardComponent } from './mpc-card.component';

describe('MpcCardComponent', () => {
  let component: MpcCardComponent;
  let fixture: ComponentFixture<MpcCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpcCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpcCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
