import {Pipe, PipeTransform} from '@angular/core';
import {Mpc} from '../../shared/models/mpc.model';

@Pipe({
  name: 'mpcListFilter'
})
export class MpcListFilterPipe implements PipeTransform {

  transform(mpcs: Mpc[], filterValue: string): Mpc[] {
    if (!mpcs) {
      return [];
    }
    if (!filterValue) {
      return mpcs;
    }

    filterValue = filterValue.toLowerCase();

    return mpcs.filter(mpc => {
      return mpc.name.toLowerCase().includes(filterValue) ||
        mpc.author.toLowerCase().includes(filterValue) ||
        mpc.describe.toLowerCase().includes(filterValue);
    });
  }
}
