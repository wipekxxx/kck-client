import {Pipe, PipeTransform} from '@angular/core';
import {Mpc} from '../../shared/models/mpc.model';

@Pipe({
  name: 'userMpcList'
})
export class UserMpcListPipe implements PipeTransform {

  transform(mpcs: Mpc[], userId: string): Mpc[] {
    if (!mpcs) {
      return [];
    }
    if (!userId) {
      return mpcs;
    }

    userId = userId.toLowerCase();

    return mpcs.filter(mpc => mpc.user.id === userId);
  }
}
