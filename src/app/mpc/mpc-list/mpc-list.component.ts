import {AfterViewInit, ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {MpcConsoleService} from '../../services/mpc-console.service';
import {Observable} from 'rxjs/Observable';
import {Mpc} from '../../shared/models/mpc.model';
import {NgModel} from '@angular/forms';
import {UsersService} from '../../services/users.service';
import {LOCAL_STORAGE_GMAIL_USER_FIELD_NAME} from '../../app.consts';
import {SocialUser} from 'angular5-social-login';

@Component({
  changeDetection: ChangeDetectionStrategy.Default,
  selector: 'app-mpc-list',
  templateUrl: './mpc-list.component.html',
  styleUrls: ['./mpc-list.component.scss']
})
export class MpcListComponent implements AfterViewInit{

  mpcs$: Observable<Mpc[]>;
  mpcsToFilter: Mpc[];

  filterModel: '';
  filterValue: string = '';
  tags: string[] = [];
  tagFilter: string = '';

  isUserLogged = false;
  userId = '';

  user: SocialUser;

  constructor(private mpcConsoleService: MpcConsoleService) {
    this.init();
  }

  private init() {
    this.mpcs$ = this.mpcConsoleService.getAll();
    this.mpcs$.subscribe((mpcs: Mpc[]) => {
      mpcs.forEach(mpc => {
        if (mpc.tags.length > 0) {
          mpc.tags.forEach(tag => {
            this.tags.push(tag['name']);
          });
          this.tags = [...Array.from(new Set(this.tags))];
        }
      });
      this.mpcsToFilter = mpcs;
    });

    this.user = JSON.parse(localStorage.getItem(LOCAL_STORAGE_GMAIL_USER_FIELD_NAME));

    this.isUserLogged = this.user != null;
  }

  filter(filterValue: string) {
    this.filterValue = filterValue;
  }

  ngAfterViewInit(): void {
  }

  clearFilter() {
    this.filterValue = '';
    this.tagFilter = '';
    this.filterModel = '';
    this.userId = '';
  }

  showUserMpcs() {
    this.clearFilter();
    this.userId = this.user.id;
  }

  showByTag(tag: string) {
    this.clearFilter();
    this.tagFilter = tag;
  }

  refreshList() {
    setTimeout(() => {
      this.init();
    }, 100);
  }
}
