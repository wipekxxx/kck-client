import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpcEditPropertiesModalComponent } from './mpc-edit-properties-modal.component';

describe('MpcEditPropertiesModalComponent', () => {
  let component: MpcEditPropertiesModalComponent;
  let fixture: ComponentFixture<MpcEditPropertiesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpcEditPropertiesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpcEditPropertiesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
