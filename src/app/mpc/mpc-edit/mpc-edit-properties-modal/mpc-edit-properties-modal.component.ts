import {Component, OnInit} from '@angular/core';
import {MpcPropertiesModalComponent} from '../../new-mpc/mpc-properties-modal/mpc-properties-modal.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent, MatDialogRef} from '@angular/material';
import {MyErrorStateMatcher} from '../../../shared/validators/error-matcher.validator';
import {NewMpcService} from '../../../services/new-mpc.service';
import {EditMpcService} from '../../../services/edit-mpc.service';

@Component({
  selector: 'app-mpc-edit-properties-modal',
  templateUrl: './mpc-edit-properties-modal.component.html',
  styleUrls: ['./mpc-edit-properties-modal.component.scss']
})
export class MpcEditPropertiesModalComponent implements OnInit {

  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = true;

  separatorKeysCodes = [ENTER, COMMA];

  form: FormGroup;

  matcher: MyErrorStateMatcher = new MyErrorStateMatcher();

  tags: string[] = [];

  constructor(public dialogRef: MatDialogRef<MpcPropertiesModalComponent>, private editMpcService: EditMpcService, private fb: FormBuilder) {
    this.tags = editMpcService.mpc.tags.map(tag => tag['name']);
    this.form = fb.group({
      name: [editMpcService.mpc.name, Validators.compose([Validators.required])],
      tags: [editMpcService.mpc.tags.map(tag => tag['name'])],
      describe: [editMpcService.mpc.describe, Validators.compose([Validators.required])],
      author: [editMpcService.mpc.author, Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
  }

  saveMpc() {
    this.editMpcService.mpc.name = this.form.controls.name.value;
    this.editMpcService.mpc.describe = this.form.controls.describe.value;
    this.editMpcService.mpc.author = this.form.controls.author.value;
    this.editMpcService.mpc.tags = this.tags;
    this.dialogRef.close();
    this.editMpcService.saveMpc();
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.tags.push(value.trim());
    }

    if (input) {
      input.value = '';
    }
  }

  remove(tag: any): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

}
