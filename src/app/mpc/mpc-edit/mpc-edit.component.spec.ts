import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpcEditComponent } from './mpc-edit.component';

describe('MpcEditComponent', () => {
  let component: MpcEditComponent;
  let fixture: ComponentFixture<MpcEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpcEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpcEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
