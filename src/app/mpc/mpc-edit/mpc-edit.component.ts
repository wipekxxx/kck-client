import { Component, OnInit } from '@angular/core';
import {MPC_KEYS} from '../mpc.consts';
import {NewMpcService} from '../../services/new-mpc.service';
import {MatDialog} from '@angular/material';
import {MpcPropertiesModalComponent} from '../new-mpc/mpc-properties-modal/mpc-properties-modal.component';
import {MpcConsoleService} from '../../services/mpc-console.service';
import {Mpc} from '../../shared/models/mpc.model';
import {ActivatedRoute} from '@angular/router';
import {MpcEditPropertiesModalComponent} from './mpc-edit-properties-modal/mpc-edit-properties-modal.component';
import {EditMpcService} from '../../services/edit-mpc.service';

@Component({
  selector: 'app-mpc-edit',
  templateUrl: './mpc-edit.component.html',
  styleUrls: ['./mpc-edit.component.scss']
})
export class MpcEditComponent {

  MPC_KEYS = MPC_KEYS;
  isMpcEmpty = true;
  mpc: Mpc;

  constructor(public dialog: MatDialog, private editMpcService: EditMpcService, private mpcConsoleService: MpcConsoleService, private route: ActivatedRoute) {
    this.editMpcService.padsMap.clear();

    this.route.params.subscribe(params => {
      this.mpcConsoleService.getMpcById(params['id'])
        .subscribe(data => {
          this.mpc = data;
          this.editMpcService.mpc = this.mpc;
          this.editMpcService.preparePadMaps(this.mpc);
          this.refreshMpc();
        })

    });
  }

  refreshMpc() {
    this.isMpcEmpty = this.editMpcService.padsMap.size === 0;
  }

  showMpcPropertiesDialog() {
    this.dialog.open(MpcEditPropertiesModalComponent, {
      width: '100%',
      data: null
    });
  }
}
