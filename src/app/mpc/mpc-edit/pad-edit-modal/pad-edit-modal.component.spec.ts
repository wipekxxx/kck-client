import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PadEditModalComponent } from './pad-edit-modal.component';

describe('PadEditModalComponent', () => {
  let component: PadEditModalComponent;
  let fixture: ComponentFixture<PadEditModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PadEditModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PadEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
