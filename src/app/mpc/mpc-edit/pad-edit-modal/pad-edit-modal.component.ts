import {Component, Inject, OnInit} from '@angular/core';
import {PadModalComponent} from '../../new-mpc/pad-modal/pad-modal.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {MyErrorStateMatcher} from '../../../shared/validators/error-matcher.validator';
import {FileValidators} from '../../../shared/validators/file.validator';
import {NewMpcService} from '../../../services/new-mpc.service';
import {editFileTypeValidator, fileTypeValidator} from '../../../shared/validators/file-type.validator';
import {InputFileComponent} from '../../../shared/components/input-file/input-file.component';
import {FileInput} from '../../../shared/models/file-input.model';
import {EditMpcService} from '../../../services/edit-mpc.service';

@Component({
  selector: 'app-pad-edit-modal',
  templateUrl: './pad-edit-modal.component.html',
  styleUrls: ['./pad-edit-modal.component.scss']
})
export class PadEditModalComponent implements OnInit {

  form: FormGroup;

  matcher: MyErrorStateMatcher = new MyErrorStateMatcher();
  isPlayed: boolean;
  private sample: any;
  canBeRemoved = false;

  constructor(public dialogRef: MatDialogRef<PadModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string,
              private editMpcService: EditMpcService,
              private fb: FormBuilder) {
    const pad = editMpcService.padsMap.get(data);
    let padValue = data;
    if (pad) {
      padValue = pad ? pad.padText : data;
      if (pad.base64encodedFile) {
        this.sample = new Audio(`data:audio/ogg;base64,${pad.base64encodedFile}`);
      }
      this.canBeRemoved = true;
    }
    let sampleFile: any = '';
    if (pad && pad.file) {
      sampleFile = pad.file.files;
    } else {
      if (pad) {
        pad.file = new FileInput([], pad ? pad.base64encodedFile : data);
      }
      sampleFile = pad ? pad.file.files : new FileInput([], '');
    }
    this.form = fb.group({
      padChar: [data],
      padText: [padValue, Validators.compose([Validators.maxLength(12)])],
      file: [{ value: sampleFile },
        Validators.compose([FileValidators.maxContentEditSize(104857600), editFileTypeValidator])]
    });
    this.form.controls.file.valueChanges.subscribe(newVal => {
      this.sample = new Audio(`data:audio/ogg;base64,${newVal.base64encodedFile}`);
    });
  }

  playSample() {
    if (this.sample) {
      this.sample.play();
      this.isPlayed = true;
      this.sample.onended = (e) => {
        this.isPlayed = false;
      }
    }
  }

  stopSample() {
    this.sample.pause();
    this.isPlayed = false;
  }

  ngOnInit() {
  }

  submitForm() {
    this.editMpcService.padsMap.set(this.data, this.form.value);
    this.dialogRef.close();
  }

  removePad() {
    this.editMpcService.padsMap.delete(this.data);
    this.dialogRef.close();
  }

}
