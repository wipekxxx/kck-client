import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpcEditPadComponent } from './mpc-edit-pad.component';

describe('MpcEditPadComponent', () => {
  let component: MpcEditPadComponent;
  let fixture: ComponentFixture<MpcEditPadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpcEditPadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpcEditPadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
