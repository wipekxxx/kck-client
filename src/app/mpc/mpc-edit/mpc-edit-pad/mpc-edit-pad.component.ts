import {AfterViewInit, Component, EventEmitter, Input, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {PadEditModalComponent} from '../pad-edit-modal/pad-edit-modal.component';
import {EditMpcService} from '../../../services/edit-mpc.service';

@Component({
  selector: 'app-mpc-edit-pad',
  templateUrl: './mpc-edit-pad.component.html',
  styleUrls: ['./mpc-edit-pad.component.scss']
})
export class MpcEditPadComponent implements AfterViewInit {

  @Input('padChar')
  padChar: string;

  @Output()
  padEdited = new EventEmitter<null>();

  isEdited = false;
  padText: string;

  constructor(public dialog: MatDialog, private editMpcService: EditMpcService) {

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.padText = 'Text ' + this.padChar;
      const pad = this.editMpcService.padsMap.get(this.padChar);
      if (pad) {
        this.isEdited = true;
        this.padText = pad.padText;
      } else {
        this.isEdited = false;
      }
    }, 100);
  }

  openDialog() {
    const dialogRef = this.dialog.open(PadEditModalComponent, {
      width: '100%',
      data: this.padChar
    });

    dialogRef.afterClosed().subscribe(result => {
      this.ngAfterViewInit();
      this.padEdited.emit(null);
    });
  }

}
