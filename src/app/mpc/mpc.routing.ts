import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {NewMpcComponent} from './new-mpc/new-mpc.component';
import {MpcConsoleComponent} from './mpc-console/mpc-console.component';
import {AuthGuard} from '../shared/guards/auth.guard';
import {MpcEditComponent} from './mpc-edit/mpc-edit.component';

const routes: Routes = [
  {
    path: 'mpc/new',
    component: NewMpcComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'mpc/:id',
    component: MpcConsoleComponent
  },
  {
    path: 'mpc/edit/:id',
    component: MpcEditComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class MpcRoutingModule {
}
