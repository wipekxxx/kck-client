import {AfterViewInit, Component, OnInit} from '@angular/core';
import {MPC_KEYS} from '../mpc.consts';
import {MatDialog} from '@angular/material';
import {MpcPropertiesModalComponent} from './mpc-properties-modal/mpc-properties-modal.component';
import {NewMpcService} from '../../services/new-mpc.service';

@Component({
  selector: 'app-new-mpc',
  templateUrl: './new-mpc.component.html',
  styleUrls: ['./new-mpc.component.scss']
})
export class NewMpcComponent {

  MPC_KEYS = MPC_KEYS;
  isMpcEmpty = true;

  constructor(public dialog: MatDialog, private newMpcService: NewMpcService) {
    this.newMpcService.padsMap.clear();
    this.refreshMpc();
  }

  refreshMpc() {
    this.isMpcEmpty = this.newMpcService.padsMap.size === 0;
  }

  showMpcPropertiesDialog() {
    this.dialog.open(MpcPropertiesModalComponent, {
      width: '100%',
      data: null
    });
  }
}
