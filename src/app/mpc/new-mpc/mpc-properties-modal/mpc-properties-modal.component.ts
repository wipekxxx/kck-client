import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MyErrorStateMatcher} from '../../../shared/validators/error-matcher.validator';
import {NewMpcService} from '../../../services/new-mpc.service';
import {MatChipInputEvent, MatDialogRef} from '@angular/material';
import {LOCAL_STORAGE_GMAIL_USER_FIELD_NAME} from '../../../app.consts';
import {COMMA, ENTER} from '@angular/cdk/keycodes';

@Component({
  selector: 'app-mpc-properties-modal',
  templateUrl: './mpc-properties-modal.component.html',
  styleUrls: ['./mpc-properties-modal.component.scss']
})
export class MpcPropertiesModalComponent implements OnInit {

  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = true;

  separatorKeysCodes = [ENTER, COMMA];

  form: FormGroup;

  matcher: MyErrorStateMatcher = new MyErrorStateMatcher();

  tags: string[] = [];

  constructor(public dialogRef: MatDialogRef<MpcPropertiesModalComponent>, private newMpcService: NewMpcService, private fb: FormBuilder) {
    const userName = JSON.parse(localStorage.getItem(LOCAL_STORAGE_GMAIL_USER_FIELD_NAME)).name;
    this.form = fb.group({
      name: ['Testowa nazwa', Validators.compose([Validators.required])],
      tags: [''],
      describe: ['Testowy opis', Validators.compose([Validators.required])],
      author: [userName, Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
  }

  saveMpc() {
    this.newMpcService.mpc.name = this.form.controls.name.value;
    this.newMpcService.mpc.describe = this.form.controls.describe.value;
    this.newMpcService.mpc.author = this.form.controls.author.value;
    this.newMpcService.mpc.tags = this.tags;
    this.dialogRef.close();
    this.newMpcService.saveMpc();
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.tags.push(value.trim());
    }

    if (input) {
      input.value = '';
    }
  }

  remove(tag: any): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }
}
