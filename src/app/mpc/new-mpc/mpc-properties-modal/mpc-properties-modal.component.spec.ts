import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpcPropertiesModalComponent } from './mpc-properties-modal.component';

describe('MpcPropertiesModalComponent', () => {
  let component: MpcPropertiesModalComponent;
  let fixture: ComponentFixture<MpcPropertiesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpcPropertiesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpcPropertiesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
