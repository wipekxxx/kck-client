import {AfterViewInit, ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {PadModalComponent} from '../pad-modal/pad-modal.component';
import {NewMpcService} from '../../../services/new-mpc.service';

@Component({
  selector: 'app-mpc-pad',
  templateUrl: './mpc-pad.component.html',
  styleUrls: ['./mpc-pad.component.scss']
})
export class MpcPadComponent implements AfterViewInit {

  @Input('padChar')
  padChar: string;

  @Output()
  padEdited = new EventEmitter<null>();

  isEdited = false;
  padText: string;

  constructor(public dialog: MatDialog, private newMpcService: NewMpcService) {

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.padText = 'Text ' + this.padChar;
      const pad = this.newMpcService.padsMap.get(this.padChar);
      if (pad) {
        this.isEdited = true;
        this.padText = pad.padText;
      } else {
        this.isEdited = false;
      }
    }, 1);
  }

  openDialog() {
    const dialogRef = this.dialog.open(PadModalComponent, {
      width: '100%',
      data: this.padChar
    });

    dialogRef.afterClosed().subscribe(result => {
      this.ngAfterViewInit();
      this.padEdited.emit(null);
    });
  }

}
