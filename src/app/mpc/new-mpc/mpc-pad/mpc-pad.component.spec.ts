import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpcPadComponent } from './mpc-pad.component';

describe('MpcPadComponent', () => {
  let component: MpcPadComponent;
  let fixture: ComponentFixture<MpcPadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpcPadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpcPadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
