import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FileValidators} from '../../../shared/validators/file.validator';
import {NewMpcService} from '../../../services/new-mpc.service';
import {MyErrorStateMatcher} from '../../../shared/validators/error-matcher.validator';
import {fileTypeValidator} from '../../../shared/validators/file-type.validator';

@Component({
  selector: 'app-pad-modal',
  templateUrl: './pad-modal.component.html',
  styleUrls: ['./pad-modal.component.scss']
})
export class PadModalComponent implements OnInit {

  form: FormGroup;

  matcher: MyErrorStateMatcher = new MyErrorStateMatcher();
  isPlayed: boolean;
  private sample: any;
  canBeRemoved = false;

  constructor(public dialogRef: MatDialogRef<PadModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string,
              private newMpcService: NewMpcService,
              private fb: FormBuilder) {
    const pad = newMpcService.padsMap.get(data);
    let padValue = data;
    if (pad) {
      padValue = pad ? pad.padText : data;
      if (pad.file.base64encodedFile) {
        this.sample = new Audio(`data:audio/ogg;base64,${pad.file.base64encodedFile}`);
      }
      this.canBeRemoved = true;
    }
    let sampleFile: any = '';
    if (pad && pad.file) {
      sampleFile = pad.file.files;
    }
    this.form = fb.group({
      padChar: [data],
      padText: [padValue, Validators.compose([Validators.maxLength(12)])],
      file: [{ value: sampleFile, disabled: false },
        Validators.compose([Validators.required, FileValidators.maxContentSize(104857600), fileTypeValidator])]
    });

    this.form.controls.file.valueChanges.subscribe(newVal => {
      this.sample = new Audio(`data:audio/ogg;base64,${newVal.base64encodedFile}`);
    });
  }

  playSample() {
    if (this.sample) {
      this.sample.play();
      this.isPlayed = true;
      this.sample.onended = (e) => {
        this.isPlayed = false;
      }
    }
  }

  stopSample() {
    this.sample.pause();
    this.isPlayed = false;
  }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  submitForm() {
    this.newMpcService.padsMap.set(this.data, this.form.value);
    this.dialogRef.close();
  }

  removePad() {
    this.newMpcService.padsMap.delete(this.data);
    this.dialogRef.close();
  }
}
