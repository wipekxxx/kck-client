import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PadModalComponent } from './pad-modal.component';

describe('PadModalComponent', () => {
  let component: PadModalComponent;
  let fixture: ComponentFixture<PadModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PadModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PadModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
