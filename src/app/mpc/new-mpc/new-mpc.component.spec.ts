import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewMpcComponent } from './new-mpc.component';

describe('NewMpcComponent', () => {
  let component: NewMpcComponent;
  let fixture: ComponentFixture<NewMpcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewMpcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewMpcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
