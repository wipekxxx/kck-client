import {Component, OnInit} from '@angular/core';
import {MpcConsoleService} from '../../services/mpc-console.service';
import {Mpc} from '../../shared/models/mpc.model';
import {Observable} from 'rxjs/Observable';
import {Pad} from '../../shared/models/pad.model';
import {ActivatedRoute} from '@angular/router';
import {MPC_KEYS} from '../mpc.consts';

@Component({
  selector: 'app-mpc-console',
  templateUrl: './mpc-console.component.html',
  styleUrls: ['./mpc-console.component.scss']
})
export class MpcConsoleComponent implements OnInit {

  isLoaded = false;
  panelOpenState = false;

  mpc: Mpc;
  pads: Pad[];

  constructor(private mpcConsoleService: MpcConsoleService, private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.mpcConsoleService.getMpcById(params['id'])
        .subscribe(data => {
          this.mpc = data;
          this.mpc.pads = this.preparePads(this.mpc.pads);
          // this.pads = this.mpcConsoleService.getPadsByMpcId(params['id'], data.padsChars);
          this.isLoaded = true;
        });
    });
    // setTimeout(() => {}, 10000);
  }

  ngOnInit() {
  }

  private preparePads(pads: Pad[]): Pad[] {
    const preparedPads = Array(MPC_KEYS.length);
    const padChars = pads.map(pad => pad.padChar);

    for (let i = 0; i < MPC_KEYS.length; i++) {
      if (padChars.includes(MPC_KEYS[i])) {
        const pad = pads.filter(p => p.padChar === MPC_KEYS[i]);
        preparedPads[i] = {
          padChar: MPC_KEYS[i],
          padText: pad[0].padText,
          base64encodedFile: pad[0].base64encodedFile
        };
      } else {
        preparedPads[i] = {
          padChar: MPC_KEYS[i],
          padText: 'Brak'
        };
      }
    }

    return preparedPads;
  }

}
