import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpcConsolePadComponent } from './mpc-console-pad.component';

describe('MpcConsolePadComponent', () => {
  let component: MpcConsolePadComponent;
  let fixture: ComponentFixture<MpcConsolePadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpcConsolePadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpcConsolePadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
