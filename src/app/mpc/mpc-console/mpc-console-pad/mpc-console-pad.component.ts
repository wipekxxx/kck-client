import {Component, HostListener, Input, OnInit} from '@angular/core';
import {Pad} from '../../../shared/models/pad.model';

@Component({
  selector: 'app-mpc-console-pad',
  templateUrl: './mpc-console-pad.component.html',
  styleUrls: ['./mpc-console-pad.component.scss']
})
export class MpcConsolePadComponent implements OnInit {

  @Input('pad')
  pad: Pad;

  isPlayed = false;
  isActive = false;

  private _audio: any;

  constructor() {

  }

  ngOnInit() {
    this.isActive = this.pad.base64encodedFile != null && this.pad.padText != null;
  }

  play() {
    if (this.pad.base64encodedFile) {
      if (!this.isPlayed) {
        this._audio = new Audio(`data:audio/ogg;base64,${this.pad.base64encodedFile}`);
        this._audio.play();
        this.isPlayed = true;
        this._audio.onended = (e) => {
          this.isPlayed = false;
        }
      } else {
        this.stop();
      }
    } else {
      console.log('No sound binded');
    }
  }

  stop() {
    this._audio.pause();
    this.isPlayed = false;
  }

  @HostListener('window:keypress', ['$event'])
  onKeyPressed(evt: KeyboardEvent) {
    if (this.pad != null && evt.key.toLowerCase() === this.pad.padChar.toLowerCase()) {
      if (!this.isPlayed) {
        this.play();
      } else {
        this.stop();
      }
    }
  }

}
