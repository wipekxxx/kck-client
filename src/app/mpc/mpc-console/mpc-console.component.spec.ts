import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpcConsoleComponent } from './mpc-console.component';

describe('MpcConsoleComponent', () => {
  let component: MpcConsoleComponent;
  let fixture: ComponentFixture<MpcConsoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpcConsoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpcConsoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
