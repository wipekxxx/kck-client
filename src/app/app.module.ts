import {BrowserModule} from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {AppComponent} from './app.component';
import {SharedModule} from './shared/shared.module';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {LayoutModule} from './layout/layout.module';
import {HomeModule} from './home/home.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app.routing';
import {MpcModule} from './mpc/mpc.module';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {AuthServiceConfig, GoogleLoginProvider, SocialLoginModule} from 'angular5-social-login';
import {UsersService} from './services/users.service';
import {GMAIL_CLIENT_ID} from './app.consts';
import {AuthInterceptor} from './shared/http/auth.interceptor';
import {AuthGuard} from './shared/guards/auth.guard';

export function getAuthServiceConfigs() {
  return new AuthServiceConfig([
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider(GMAIL_CLIENT_ID)
    }
  ]);
}

export function JwtModuleConfigFactory() {
  return localStorage.getItem('access_token');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    AppRoutingModule,
    RouterModule,
    LayoutModule,
    HomeModule,
    HttpClientModule,
    MpcModule,
    MDBBootstrapModule.forRoot(),
    SocialLoginModule
  ],
  exports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    LayoutModule,
    HomeModule,
    AppRoutingModule,
    MpcModule
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    UsersService,
    AuthGuard
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {
}
