import { TestBed, inject } from '@angular/core/testing';

import { SaveMpcService } from './save-mpc.service';

describe('SaveMpcService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SaveMpcService]
    });
  });

  it('should be created', inject([SaveMpcService], (service: SaveMpcService) => {
    expect(service).toBeTruthy();
  }));
});
