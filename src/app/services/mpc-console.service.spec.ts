import { TestBed, inject } from '@angular/core/testing';

import { MpcConsoleService } from './mpc-console.service';

describe('MpcConsoleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MpcConsoleService]
    });
  });

  it('should be created', inject([MpcConsoleService], (service: MpcConsoleService) => {
    expect(service).toBeTruthy();
  }));
});
