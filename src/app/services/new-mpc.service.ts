import {Injectable} from '@angular/core';
import {Pad} from '../shared/models/pad.model';
import {Mpc} from '../shared/models/mpc.model';
import {SaveMpcService} from './save-mpc.service';

@Injectable()
export class NewMpcService {

  padsMap = new Map<string, Pad>();

  mpc: Mpc;

  constructor(private saveMpcService: SaveMpcService) {
    this.mpc = new Mpc();
    this.mpc.pads = Array();
    this.mpc.id = this.generateId();
  }

  saveMpc() {
    this.padsMap.forEach((v, k) => {
      v.id = this.generateId();
      this.mpc.pads.push(v);
    });
    this.padsMap = new Map<string, Pad>();
    this.saveMpcService.save(this.mpc);
  }

  private generateId(): string {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < 11; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  preparePadMaps(mpc: Mpc) {
    mpc.pads.forEach(pad => this.padsMap.set(pad.padChar, pad));
  }
}
