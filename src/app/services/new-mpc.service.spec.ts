import { TestBed, inject } from '@angular/core/testing';

import { NewMpcService } from './new-mpc.service';

describe('NewMpcService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewMpcService]
    });
  });

  it('should be created', inject([NewMpcService], (service: NewMpcService) => {
    expect(service).toBeTruthy();
  }));
});
