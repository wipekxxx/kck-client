import {Injectable} from '@angular/core';
import {Mpc} from '../shared/models/mpc.model';
import {HttpClient} from '@angular/common/http';
import {Pad} from '../shared/models/pad.model';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';
import {SocialUser} from 'angular5-social-login';
import {LOCAL_STORAGE_GMAIL_USER_FIELD_NAME} from '../app.consts';
import {Tag} from '../shared/models/tag.model';

@Injectable()
export class SaveMpcService {

  constructor(private http: HttpClient, private router: Router) {
  }

  save(mpc: Mpc) {
    this.saveMpc(mpc);
    // this.savePads(mpc);
    setTimeout(() => {this.router.navigate(['mpc/' + mpc.id])}, 1000);
  }

  saveMpc(mpc: Mpc) {
    const padChars = Array();

    // mpc.pads.forEach(pad => {
    //   padChars.push(pad.padChar);
    // });
    const user: SocialUser = JSON.parse(localStorage.getItem(LOCAL_STORAGE_GMAIL_USER_FIELD_NAME));
    const convertedMpc = {
        mpc: {
          id: mpc.id,
          name: mpc.name,
          describe: mpc.describe,
          author: mpc.author,
          tags: mpc.tags.map(tag => new Tag(tag)),
          pads: this.getConvertedPads(mpc.pads)
        },
        userId: user.id
    };
    this.http.post(environment.apiUrl + 'mpc', convertedMpc)
      .subscribe(data => console.log(data));
  }

  private getConvertedPads(pads: Pad[]): Pad[] {
    const convertedPads: Pad[] = [];
    pads.forEach(pad => {
      const newPad = new Pad();
      newPad.id = pad.id;
      newPad.padChar = pad.padChar;
      newPad.padText = pad.padText;
      newPad.base64encodedFile = pad.file != null ? pad.file.base64encodedFile : pad.base64encodedFile;
      convertedPads.push(newPad);
    });
    return convertedPads;
  }

  savePads(mpc: Mpc) {
    const body = new FormData();
    mpc.pads.forEach(pad => {
      const newPad = new Pad();
      newPad.padChar = pad.padChar;
      newPad.padText = pad.padText;
      newPad.base64encodedFile = pad.file.base64encodedFile;
      body.append('pad' + newPad.padChar, JSON.stringify(newPad));
    });
    // mpc.pads.forEach(data => console.log(data));
    // mpc.pads.map(pad => pad.file = encoder.getBase64EncodedString(pad.file['files'][0]));

    this.http.post(environment.apiUrl + 'mpc/pads/' + mpc.id, body)
      .subscribe(data => console.log(data));
  }

  update(mpc: Mpc) {
    const user: SocialUser = JSON.parse(localStorage.getItem(LOCAL_STORAGE_GMAIL_USER_FIELD_NAME));
    const convertedMpc = {
      mpc: {
        id: mpc.id,
        name: mpc.name,
        describe: mpc.describe,
        author: mpc.author,
        tags: mpc.tags.map(tag => new Tag(tag)),
        pads: this.getConvertedPads(mpc.pads)
      },
      userId: user.id
    };
    this.http.post(environment.apiUrl + 'mpc/update', convertedMpc)
      .subscribe(data => console.log(data));
    setTimeout(() => {this.router.navigate(['mpc/' + mpc.id])}, 1000);
  }
}
