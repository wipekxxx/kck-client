import { TestBed, inject } from '@angular/core/testing';

import { EditMpcService } from './edit-mpc.service';

describe('EditMpcService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EditMpcService]
    });
  });

  it('should be created', inject([EditMpcService], (service: EditMpcService) => {
    expect(service).toBeTruthy();
  }));
});
