import {Injectable} from '@angular/core';
import {AuthService as SocialAuthService, GoogleLoginProvider, SocialUser} from 'angular5-social-login';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LOCAL_STORAGE_GMAIL_USER_FIELD_NAME, TOKEN_AUTH_PASSWORD, TOKEN_AUTH_USERNAME, TOKEN_NAME} from '../app.consts';
import {environment} from '../../environments/environment';
import {JwtHelper, tokenNotExpired} from 'angular2-jwt';
import {Router} from '@angular/router';


@Injectable()
export class UsersService {
  jwtHelper: JwtHelper = new JwtHelper();
  accessToken: string;
  logged = false;
  user: any;

  constructor(private socialAuthService: SocialAuthService, private http: HttpClient) {
    // this.accessToken = localStorage.getItem(TOKEN_NAME);
    // this.logged = tokenNotExpired(this.accessToken);
    // if (this.logged) {
    //   const decodedToken = this.jwtHelper.decodeToken(this.accessToken);
    //   console.log(decodedToken);
    // }
    const user = JSON.parse(localStorage.getItem(LOCAL_STORAGE_GMAIL_USER_FIELD_NAME));
    this.logged = user != null;
    if (this.logged) {
      this.register(user, false);
    }
  }

  isUserLogged(): boolean {
    return this.logged;
  }

  public gmailLogin() {
    const socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (user: SocialUser) => {
        localStorage.setItem(LOCAL_STORAGE_GMAIL_USER_FIELD_NAME, JSON.stringify(user));
        this.register(user, true);
        // this.loginApi(user);
      }
    );
  }

  loginApi(user: SocialUser): void {
    const body = `username=${encodeURIComponent(user.name)}&password=${encodeURIComponent('12345')}&grant_type=password`;
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD));

    this.http.post(environment.authApiUrl, body, {headers: headers, observe: 'response'})
      .subscribe(onSuccess => {
          console.log(onSuccess);
          localStorage.setItem(TOKEN_NAME, JSON.stringify(onSuccess));
          this.logged = true;
          window.location.reload();

        }, onFail => {
          console.log(onFail);
          this.logged = false;
        }
      );
  }

  logout() {
    localStorage.removeItem(LOCAL_STORAGE_GMAIL_USER_FIELD_NAME);
  }

  private register(user: SocialUser, redirect: boolean) {
    const body = {
      id: user.id,
      username: user.name,
      email: user.email
    };
    this.http.post(environment.apiUrl + 'users/register', body)
      .subscribe(data => {
        this.logged = true;
        if (redirect) {
          window.location.reload();
        }
      },
        onFail => console.log(onFail));
  }
}
