import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Mpc} from '../shared/models/mpc.model';
import {Pad} from '../shared/models/pad.model';
import {MPC_KEYS} from '../mpc/mpc.consts';
import {environment} from '../../environments/environment';

@Injectable()
export class MpcConsoleService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Mpc[]> {
    return this.http.get<Mpc[]>(environment.apiUrl + 'mpc');
  }

  getMpcById(id: string): Observable<Mpc> {
    return this.http.get<Mpc>(environment.apiUrl + 'mpc/' + id);
  }

  removeMpcById(id: string) {
    this.http.delete(environment.apiUrl + 'mpc/' + id).subscribe(data => console.log(data));
  }

  getPadsByMpcId(id: string, padChars: string[]): Pad[] {
    const pads = Array(MPC_KEYS.length);

    for (let i = 0; i < MPC_KEYS.length; i++) {
      if (padChars.includes(MPC_KEYS[i])) {
        this.http.get<Pad[]>(environment.apiUrl + 'mpc/pads/' + id + '/' + MPC_KEYS[i].charCodeAt(0))
          .subscribe(data => {
            pads[i] = data;
          });
      } else {
        pads[i] = {
          padChar: MPC_KEYS[i],
          padText: 'Brak'
        };
      }
    }

    // MPC_KEYS.forEach(key => {
    //   if (padChars.includes(key)) {
    //     this.http.get<Pad[]>(API_URL + 'mpc/pads/' + id + '/' + key)
    //       .subscribe(data => {
    //         console.log(data);
    //         pads.push(data)
    //       });
    //   } else {
    //     pads.push({
    //       padChar: key,
    //       padText: 'Text ' + key
    //     });
    //   }
    // });

    // padChars.forEach(ch => {
    //   this.http.get<Pad[]>(API_URL + 'mpc/pads/' + id + '/' + ch)
    //     .subscribe(data => {
    //       console.log(data);
    //       pads.push(data)
    //     });
    // });

    return pads;
  }
}
