import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../services/users.service';
import {LOCAL_STORAGE_GMAIL_USER_FIELD_NAME} from '../../app.consts';
import {SocialUser} from 'angular5-social-login';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  isUsersLogged = false;

  constructor(private usersService: UsersService) { }

  ngOnInit() {
    const user: SocialUser = JSON.parse(localStorage.getItem(LOCAL_STORAGE_GMAIL_USER_FIELD_NAME));
    this.isUsersLogged = user != null;
  }

  login() {
    this.usersService.gmailLogin();
  }

  getLoggedUserName(): string {
    const user: SocialUser = JSON.parse(localStorage.getItem(LOCAL_STORAGE_GMAIL_USER_FIELD_NAME));
    return user != null ? user.name : null;
  }

}
